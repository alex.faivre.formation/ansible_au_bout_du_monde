TODO : 

Créer dans un template une fiche information de déploiement pour informer un utilisateur que son service est disponible avec les infos suivantes :

- Date du jour de déploiement
- Les versions des applications
- Le point de contact en cas de problème
- Le mot de passe pour se connecter à son application (généré dynamiquement)