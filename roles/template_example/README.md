# template_example

[![Ansible version](https://img.shields.io/badge/ansible-%3E%3D2.15-black.svg?style=flat-square&logo=ansible)](https://github.com/ansible/ansible)


Présentation des fonctions des templates

**Platforms Supported**:

| Platform | Versions |
|----------|----------|
| Ubuntu | 24.04 |

## ⚠️ Requirements

Ansible >= 2.15.

### Ansible role dependencies

None.

## ⚡ Example playbook 

Basic usage is:

```yaml
- hosts: all
  roles:
    - role: template_example
      vars:
        contact_point: Alex FAIVRE
        demo: basic
        output_folder: templates_output
        packages:
          - name: postgres
            version: 15
          - name: postgres-server
            version: 15
        telephone:
          nom: Alex
          nouvelle_entree: test
          numero:
            - '0625252525'
            - '00000000'
        templates:
          - dest: boucles.txt
            executable: true
            src: template.j2
          - dest: script_python.py
            executable: true
            src: script_python.j2
          - dest: documentation.txt
            executable: true
            src: documentation.j2
        vault_config:
          engine_mount_point: ansible
          secret: ansible
          vault_url: http://172.30.30.22:8200
        
```

## ⚙️ Role Variables

Variables are divided in three types.

The **default vars** section shows you which variables you may
override in your ansible inventory. As a matter of fact, all variables should
be defined there for explicitness, ease of documentation as well as overall
role manageability.

The **context variables** are shown in section below hint you
on how runtime context may affects role execution.

### Default variables
Role default variables from `defaults/main.yml`.

| Variable Name | Value |
|---------------|-------|
| telephone | nom: Alex<br>nouvelle_entree: test<br>numero:<br>- '0625252525'<br>- '00000000'<br> |
| output_folder | templates_output |
| templates | - dest: boucles.txt<br>  executable: true<br>  src: template.j2<br>- dest: script_python.py<br>  executable: true<br>  src: script_python.j2<br>- dest: documentation.txt<br>  executable: true<br>  src: documentation.j2<br> |
| demo | basic |
| packages | - name: postgres<br>  version: 15<br>- name: postgres-server<br>  version: 15<br> |
| contact_point | Alex FAIVRE |
| vault_config | engine_mount_point: ansible<br>secret: ansible<br>vault_url: http://172.30.30.22:8200<br> |

### Context variables

Those variables from `vars/*.{yml,json}` are loaded dynamically during task
runtime using the `include_vars` module.

Variables loaded from `vars/main.yml`.




## Author Information

Alex Faivre Formation