# postgres

[![Ansible version](https://img.shields.io/badge/ansible-%3E%3D2.15-black.svg?style=flat-square&logo=ansible)](https://github.com/ansible/ansible)


your role description

**Platforms Supported**:

None.

## ⚠️ Requirements

Ansible >= 2.15.

### Ansible role dependencies

None.

## ⚡ Example playbook 

Basic usage is:

```yaml
- hosts: all
  roles:
    - role: postgres
      vars:
        packages:
          - curl
        
```

## ⚙️ Role Variables

Variables are divided in three types.

The **default vars** section shows you which variables you may
override in your ansible inventory. As a matter of fact, all variables should
be defined there for explicitness, ease of documentation as well as overall
role manageability.

The **context variables** are shown in section below hint you
on how runtime context may affects role execution.

### Default variables
Role default variables from `defaults/main.yml`.

| Variable Name | Value |
|---------------|-------|
| packages | - curl<br> |

### Context variables

Those variables from `vars/*.{yml,json}` are loaded dynamically during task
runtime using the `include_vars` module.

Variables loaded from `vars/main.yml`.

| Variable Name | Value |
|---------------|-------|
| packages | - postgresql<br>- postgresql-client<br>- libpq-dev<br>- acl<br> |
| pip_packages | - psycopg2<br> |



## Author Information

Alex FAIVRE Formation