# Ansible au bout du monde

Contenu de la formation Ansible

Plusieurs roles sont dispo dans ce repo :

- template_example
- postgres

Pour lancer un role, vous pouvez lancer le playbook associé. Par exemple :

```ansible-playbook playbook-template.yml```

Si vous souhaitez surcharger une variable existante d'un role, vous pouvez le faire comme ceci :

```ansible-playbook playbook-template.yml -e contact_point="Another contact point"```

## Notes

Il faut installer les packages suivants pour faire du réseau avec Cisco XR :

Via pip3 :
 - ansible-pylibssh 
 - paramiko

Via apt :
  - sshpass